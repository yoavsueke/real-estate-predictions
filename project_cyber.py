# importing the required libraries:



# built-in module called math, which has a list of mathematical functions including The math.sqrt() ,which returns the square root of a number
import math

#pandas is a library for data manipulation and analysis
import pandas as pd

#sklearn's linear_model is a module for performing machine learning with linear modules
from sklearn import linear_model

#sklearn.model_selection's train_test_split is a function for splitting data arrays into two subsets: for training data and for testing data.
from sklearn.model_selection import train_test_split

#sklearn.datasets contains info about boston house prices in 1970 of which includes: crime rate by town, average number of rooms per residence and 11 more columns with info about the houses
from sklearn.datasets import load_boston

#sklearn.metrics (mean_squared_error)- The mean squared error (MSE) tells you how accurate your module is. It does this by finding the difference between the two outputs, squares them (to avoid minus signs),adds them together and then divides it by the amount of variables present, therefore the lower the score the better the module is. -if the score is 28.014 that means that the average squared score is 28.014 so the average difference between the predicted price and real price is 5.29 thousand which is extremely accurate
from sklearn.metrics import mean_squared_error


def main():
    """
    This is my main function.
    """

    #processing the data and building our linear module from it:

    #loading the data from sklearn.datasets
    boston = load_boston()

    #organizing the info(data frame) into x and y columns in which the x values are the 13 characteristics which define the property, and the y values is the house price
    df_x = pd.DataFrame(boston.data, columns=boston.feature_names)
    df_y = pd.DataFrame(boston.target)

    #saving the Linear regression function from the sklearn (linear_model) library as reg
    reg = linear_model.LinearRegression()

    #using the train_test_split module from sklearn.model_selection whilst using the variables df_x and df_y to divide the info into training data and testing data to compare with what the module predicted
    x_train, x_test, y_train, y_test = train_test_split(df_x, df_y, test_size=0.33, random_state=42)

    #training the module with linear_model.LinearRegression() function which was saved as reg and use the x and y training values(x_train, y_train) which train_test_split has split for us
    reg.fit(x_train, y_train)



    #checking accuracy of the module:

    #saving the price prediction our module has made according to the x testing values(x_test) so we could compare it to the real price(y_test)when we compare it in the following code lines to check how well the module worked
    predicted_price = reg.predict(x_test)

    #checking the performance and accuracy of the module with sklearn.metrics (mean_squared_error) and saving it as difference_prices_squared
    difference_prices_squared = mean_squared_error(y_test, predicted_price)




    #printing the house prices and average price difference between prediction and reality:

    #printing the price prediction our module has made so we could compare it to the real price(y_test) in the following line
    print("predicted price in thousands :")
    print(predicted_price)

    #printing the actual price (y_test) so we could compare it to what the module predicted in the previous line
    print("real price in thousands : ")
    print(y_test)

    #printing the average diffrence iin prices between our modules pridction and the actual price and we do that by square rooting what the mean_squared_error has given us which is the average difference in prices squared so to check the actual difference in thousands we square rooted the answer
    print("average price difference in thousands :")
    print(math.sqrt(difference_prices_squared))


if __name__ == '__main__':
    main()
